
				// [SECTION] While loop
/*
	- takes in an expression/condition
	- expressions are any unit of code that can be evaluated to a value
	- if the condition evaluates to true, the statements inside the block will be executed
*/


let count = 5;

while(count!==0){
	console.log("count: "+count);
	count--; //decrement count value
}



					// [SECTION] Do While

let number = Number(prompt("Give me a number: "));
// The "Number" function works similar w/ the "parseInt" function


do {
	console.log("Number: "+number);
	number += 1; //increment value into 1

}while(number < 10); //number must be less than 10



					// [SECTION] For loop

	// initialization // condition  // change of value
for(let count = 10; count<=20; count++){
	console.log(count);
}

			
let myString = "tupe";
// t = index 0
// u = index 1
// p = index2
// e = index3
console.log("myString length: "+myString.length);

console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);

for(i=0; i<myString.length;i++){
	console.log(myString[i]);
}

let myName = "AlEx";

for(let i=0; i<myName.length; i++){
	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u"
	){
		console.log(3)
	}
	else{
		console.log(myName[i]);
	}
}







